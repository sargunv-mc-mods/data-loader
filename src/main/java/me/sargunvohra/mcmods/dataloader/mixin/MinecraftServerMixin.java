package me.sargunvohra.mcmods.dataloader.mixin;

import me.sargunvohra.mcmods.dataloader.DataLoader;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.resource.*;
import net.minecraft.server.MinecraftServer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(value = MinecraftServer.class, priority = 1001) // after Fabric
public class MinecraftServerMixin {

    @Inject(method = "loadDataPacks", at = @At(value = "HEAD"))
    private static void loadDataPacks(
        ResourcePackManager<ResourcePackProfile> resourcePackManager,
        DataPackSettings dataPackSettings,
        boolean safeMode,
        CallbackInfoReturnable<DataPackSettings> info
    ) {
        resourcePackManager.providers.add(
            new FileResourcePackProvider(
                DataLoader.DATAPACKS_PATH.toFile(),
                DataLoader.RESOURCE_PACK_SOURCE
            )
        );
    }
}

