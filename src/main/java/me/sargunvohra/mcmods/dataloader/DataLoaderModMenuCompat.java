package me.sargunvohra.mcmods.dataloader;

import io.github.prospector.modmenu.api.ConfigScreenFactory;
import io.github.prospector.modmenu.api.ModMenuApi;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ConfirmScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Util;

public class DataLoaderModMenuCompat implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return (ConfigScreenFactory<Screen>) parent -> new ConfirmScreen(
            yes -> {
                if (yes) {
                    Util.getOperatingSystem().open(DataLoader.DATAPACKS_PATH.toFile());
                }
                MinecraftClient.getInstance().openScreen(parent);
            },
            new TranslatableText("dataloader.gui.confirm_open_dir.title"),
            new TranslatableText("dataloader.gui.confirm_open_dir.message")
        );
    }
}
