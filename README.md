# Data Loader

Global data packs that apply to all worlds.

## Information

Check out this mod on [CurseForge][].

## Building from source

```bash
git clone https://gitlab.com/sargunv-mc-mods/data-loader.git
cd data-loader
./gradlew build
# On Windows, use "gradlew.bat" instead of "gradlew"
```

[CurseForge]: https://minecraft.curseforge.com/projects/data-loader
